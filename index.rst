================
munin2smartphone
================

Demilitarized HTTP server eating monitoring data [1]_ and producing static pages that can be displayed securely (ie. without javascript).

.. [1] Currently supported: munin. Any stream of monitoring data should be parseable by *munin2smartphone*.

.. figure:: docs/images/screenshot_android.png

   Screen capture of *munin2smartphone* on an android phone.

   Infrastructure state is displayed in an android :ref:`web widget <smartphone_install>`.
   There is a single unfiltered warning (server name was redacted).


.. toctree::
   :caption: Table of contents
   :titlesonly:

   docs/overview
   docs/installation
   docs/running
   docs/develop
   internals <docs/source/modules>
   docs/glossary
