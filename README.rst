================
munin2smartphone
================

Demilitarized HTTP server eating monitoring data and producing static pages that can be displayed securely (ie. without javascript), on your smartphone.

.. |made-with-python| image:: https://img.shields.io/badge/Made%20with-Python-1f425f.svg
   :target: https://www.python.org/

.. |license-GPL-2| image::  https://img.shields.io/badge/license-GPL%202-informational
   :target: https://framagit.org/feth/munin2smartphone/-/blob/master/LICENSE_GPL_2.txt

.. |license-CeCILL-2.1| image::  https://img.shields.io/badge/license-CeCILL--2.1-informational
   :target: https://framagit.org/feth/munin2smartphone/-/blob/LICENSE_CeCILL_2.1.txt

.. |project-url| image:: https://img.shields.io/badge/homepage-framagit-blue
   :target: https://framagit.org/feth/munin2smartphone

.. |repository-url| image:: https://img.shields.io/badge/repository-git%2Bhttps-blue
   :target: https://framagit.org/feth/munin2smartphone.git

.. |documentation| image:: https://readthedocs.org/projects/munin2smartphone/badge/?version=latest   :alt: Read the Docs

|made-with-python| |license-GPL-2| |license-CeCILL-2.1| |project-url| |repository-url|

.. figure:: docs/images/screenshot_android.png

   Screen capture of *munin2smartphone* on an android phone.

Installation, usage and development documentation: |documentation|

documentation = "https://munin2smartphone.readthedocs.io/"

