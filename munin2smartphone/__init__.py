"""munin2widget

HTTP server converting munin-json to html reports
that you can display on your smartphone (html widget)

"""

APP_NAME = 'munin2smartphone'

APP_DESCRIPTION = (
    "HTTP server converting munin-json to html reports "
    "that you can display on your smartphone (html widget)"
)


__all__ = ("APP_NAME", "APP_DESCRIPTION")
