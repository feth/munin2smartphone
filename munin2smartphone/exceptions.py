class Munin2SmartphoneException(Exception):
    pass


class ConfigError(Munin2SmartphoneException):
    pass
