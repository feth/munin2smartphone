.. _options_config:

Running munin2smartphone
=========================

.. contents:: Table of contents

Command line options
--------------------

(section in the works)

optional arguments:
  -h, --help            show this help message and exit
  -V, --version         show program's version number and exit
  -v, --verbose         increase output verbosity -can be called multiple times: Levels: 0 time: error, 1 time: warning, 2 times: info, 3 times:debug
  --coloredlogs         terminal logs are colored (using coloredlogs): default
  --no-coloredlogs      terminal logs are NOT colored: default is colored logs
  -o OUTPUTDIR, --outputdir OUTPUTDIR
                        html output directory (default: /home/feth/munin2smartphone)
  --configfile CONFIGFILE
  --logfile LOGFILE
  --cache_directory CACHE_DIRECTORY
                        munin2smartphone cache directory (defaults to /home/feth/.cache/munin2smartphone)
  --port PORT           listening TCP port (defaults to 8765)
  --listening-address LISTENING_ADDRESS
                        listening IP address (defaults to 127.0.0.1)
  --timezone TIMEZONE   Timezone for html output -internal dates are UTC (defaults to Europe/Paris)


Configuration file
------------------

Configuration files are on the roadmap but are not handled yet.
