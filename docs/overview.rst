Overview
========

What you get
............

A non intrusive, permanent view of the important things.

You get to select what you see and what is filtered out.

Architecture
............

A *munin2smartphone* setup consists of 3 bricks:

.. topic:: a :term:`Munin` supervision server.

    We add a trigger and a bit of configuration to your stock munin installation,
    so that it pushes the state of all known checks to the next brick.

.. topic::  a *munin2smartphone* daemon.

    The daemon is receiving authenticated data and generating static web pages.

.. topic:: a frontend to view the web pages.

    Whether a web browser or an android widget.

Information flow
................

.. graphviz:: flow.dot
        
