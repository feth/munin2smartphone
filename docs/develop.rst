How to hack this project
========================

Setup your environment
----------------------

This project is managed with `poetry`, you don't need an explicit virtualenv.

* Install `poetry <https://python-poetry.org/>`_.
* then fork the project here https://framagit.org/feth/munin2smartphone (or ask me for dev status, or clone my repo and send me diffs)
* then

.. code-block:: bash

    git clone git@path.to.your/repo/munin2smartphone.git
    cd munin2smartphone
    poetry install

Run your code
-------------

Generally:

.. code-block:: bash

    poetry run <whatever is installed with the project> [options]
    poetry run munin2smartphone [options]
    poetry run python # This will run the Python version associated with the project.

Run the tests
-------------

There are no tests!
This is a shame, please help us!
