Glossary
========

.. glossary::

   Munin
        `Munin <http://munin-monitoring.org/>`_ is an IT monitoring tool. It is very easy to extend in order to monitor anything.

   RRD
        Round Robin Database - actually the concept is not used here but we use the word.

